<?php while(have_posts()) : the_post(); ?>
   <?php
    if ( has_post_thumbnail() ) {
        $urlImg = get_the_post_thumbnail_url();
    } else {
        $urlImg = '';
    }
    ?>
    <header class="welcome-container" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)),url(<?php echo $urlImg; ?>);">
        <div class="header-container">
            <div class="header-logo-link">
                <img class="header-logo img-responsive" src="<?php echo get_bloginfo('template_url') ?>/images/logo.png" alt="logo">
            </div>
            <h1 class="header-title"><?php bloginfo('name'); ?></h1>
        </div>
        <div class="welcome-content"><?php the_content(); ?></div>
        <a href="#competences" class="header-arrow-container">
            <div class="header-arrow"></div>
        </a>
    </header>
<?php endwhile;?>

