window.onload = function () {

    // fonction qui affiche ou fait disparaitre la nav au click d'un bouttton
    document.querySelector('#menu-toggle-container').addEventListener('click', function () {
        document.querySelector('#menu-toggle-container').classList.toggle('on');
        document.querySelector('#menu').parentNode.classList.toggle('show');
    }, false);

    // fonction qui affiche la fleche pour remonté en haut de la page au scroll de l'utilisateur
    document.addEventListener('scroll', function () {
        if(document.documentElement.scrollTop >= 100) {
            document.querySelector('#scroll-to-top').classList.add('show');
        } else {
            document.querySelector('#scroll-to-top').classList.remove('show')
        }
    }, false);

    document.querySelectorAll('a[href*=\\#]').forEach(function(anchor) {
        anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
    });

};