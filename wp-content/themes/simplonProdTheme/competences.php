<div class="competence-container">
    <div class="competence-content"><?php the_content(); ?></div>
    <div class="container">
<?php
$args = array('posts_per_page' => -1, 'category_name' => 'competence', 'order' => 'ASC');
$loop = new WP_Query($args);
if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
        <div class="competence-card">
            <div class="competence-card-img">
                <?php echo the_post_thumbnail(); ?>
            </div>
            <h3 class="competence-card-title"><?php echo $post->post_title; ?></h3>
            <div class="competence-card-content"><?php echo $post->post_content; ?></div>
        </div>
<?php endwhile;endif; ?>
    </div>
</div>