<?php while (have_posts()) :
the_post(); ?>
<div class="container-portfolio">
    <div class="grid-container-porfolio">
        <?php
        $gallery = get_post_gallery(get_the_ID(), false);
        $args = array(
            'post_type' => 'attachment',
            'posts_per_page' => -1,
            'post_status' => 'any',
            'post__in' => explode(',', $gallery['ids'])
        );
        $attachments = get_posts($args);

        foreach ($attachments as $attachment) {
            $image_alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
            $image_link = get_post_meta($attachment->ID, '_gallery_link_url', true);

            if (empty($image_alt)) {
                $image_alt = $attachment->post_title;
            }
            if (empty($image_alt)) {
                $image_alt = $attachment->post_excerpt;
            }
            $image_title = $attachment->post_title;
            $image_url = wp_get_attachment_image_src($attachment->ID, 'full');
            echo '<a class="card-image-portfolio" href="' . $image_link . '">';
            echo '<img src="' . $image_url[0] . '" alt="' . $image_alt . '">';
            echo '<div class="card-title-portfolio">' . $image_title . '</div>';
            echo '</a>';

        }
        endwhile; ?>
    </div>
</div>