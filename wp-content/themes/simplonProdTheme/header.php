<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <title><?php bloginfo('name'); ?> &raquo; <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="menu-toggle-container" class="menu-toggle-container">
    <div id="menu-hamburger" class="menu-hamburger"></div>
</div>

<a href="#accueil" id="scroll-to-top" class="scroll-to-top">
    <div class="scroll-to-top-arrow"></div>
</a>

<?php wp_nav_menu( array(
        'menu' =>'header-menu',
        'menu_id' => 'menu',
        'container' => 'nav',
    ) );

$menu_items = wp_get_nav_menu_items('main-nav');

if( $menu_items ) {
    foreach ($menu_items as $menu_item ) {
        $args = array(
                'p' => $menu_item->object_id,
                'post_type' => 'any'
        );

    global $wp_query;
    $wp_query = new WP_Query($args);
    $title = sanitize_title($menu_item->title);
?>
<section <?php post_class('sep'); ?> id="<?php echo $title; ?>">

    <?php if($title !== 'accueil') {  ?>
        <h2 class="title-<?php echo $title; ?> title-slide"><?php echo $title; ?></h2>
    <?php }; ?>

    <?php
    if ( have_posts() ){
        include(locate_template($title . '.php'));
    } ?>
</section>

<?php }}; ?>

