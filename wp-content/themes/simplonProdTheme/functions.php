<?php

// Ajout du script js
function add_theme_scripts() {
    wp_enqueue_script('script', get_template_directory_uri() . '/dist/app.js');
}
// Register a new navigation menu
function add_Main_Nav() {
    register_nav_menu('header-menu',__( 'Header Menu' ));
}

// fonction pour permettre de telecharger des svg
function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}

// remplace les liens par des ancres par page
function mysite_add_anchor( $atts, $item, $args ) {

    $title = get_the_title( $item->object_id);
    if(isset($title)) {
        $titleClean = sanitize_title($title);
        $atts['href'] = '#' . $titleClean;
    }
    return $atts;

}

add_filter( 'nav_menu_link_attributes', 'mysite_add_anchor', 10, 3 );

// Hook pour démarrer la fonction du menu
add_action('init', 'add_Main_Nav');

add_filter( 'nav_menu_link_attributes', 'mysite_add_anchor', 10, 3 );

// Hook pour demarrer l'ajout du script js
add_action('wp_enqueue_scripts', 'add_theme_scripts');

// Hook pour permettre de telecharger des svg
add_action('upload_mimes', 'add_file_types_to_uploads');

// Hook pour attacher des images a des articles/pages
add_theme_support('post-thumbnails');